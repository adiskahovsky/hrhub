﻿namespace Employees.Api.Services.KeyService
{
    public class KeyService : IKeyService
    {
        public string GetEmployeeLockKey(Guid id) => $"Employee_{id}";
        public IEnumerable<string> GetPositionIdKeys(IEnumerable<Guid> ids)
        {
            foreach (var id in ids)
                yield return $"Position_{id}";
        }

        public string GetPositionIdKey(Guid id) => $"Position_{id}";
    }
}
