﻿namespace Employees.Api.Services.KeyService
{
    public interface IKeyService
    {
        string GetEmployeeLockKey(Guid id);
        string GetPositionIdKey(Guid id);
        IEnumerable<string> GetPositionIdKeys(IEnumerable<Guid> ids);
    }
}