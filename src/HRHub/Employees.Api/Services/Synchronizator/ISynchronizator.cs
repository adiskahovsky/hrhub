﻿using Employees.Api.Services.Synchronizator.Models;

namespace Employees.Api.Services.Locker
{
    public interface ISynchronizator
    {
        Task<ILocker> LockAsync(string key);
        Task<ILocker> LockAsync(IEnumerable<string> keys);
    }
}
