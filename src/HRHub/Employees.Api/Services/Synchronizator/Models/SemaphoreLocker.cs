﻿namespace Employees.Api.Services.Synchronizator.Models
{
    public class SemaphoreLocker : ILocker
    {
        private readonly IDisposable[] disposables;

        public SemaphoreLocker(IDisposable disposable)
        {
            disposables = new IDisposable[] { disposable };
        }

        public SemaphoreLocker(IDisposable[] disposables)
        {
            this.disposables = disposables;
        }

        public ValueTask DisposeAsync()
        {
            foreach (var disposable in disposables)
                disposable?.Dispose();

            return ValueTask.CompletedTask;
        }
    }
}
