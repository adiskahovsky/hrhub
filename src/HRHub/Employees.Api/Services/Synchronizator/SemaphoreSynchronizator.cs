﻿using Employees.Api.Services.Synchronizator.Models;
using KeyedSemaphores;

namespace Employees.Api.Services.Locker
{
    public class SemaphoreSynchronizator : ISynchronizator
    {
        public async Task<ILocker> LockAsync(string key)
        {
            var disposable = await KeyedSemaphore.LockAsync(key);

            return new SemaphoreLocker(disposable);
        }

        public async Task<ILocker> LockAsync(IEnumerable<string> keys)
        {
            var targets = keys.Distinct().ToArray();

            var tasks = new List<Task<IDisposable>>(targets.Length);

            foreach (var target in targets)
                tasks.Add(KeyedSemaphore.LockAsync(target).AsTask());

            await Task.WhenAll(tasks);

            var disposables = new List<IDisposable>(targets.Length);

            foreach (var task in tasks)
                disposables.Add(task.Result);

            return new SemaphoreLocker(disposables.ToArray());
        }
    }
}
