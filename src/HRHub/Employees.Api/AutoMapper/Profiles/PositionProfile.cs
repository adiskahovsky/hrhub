﻿using AutoMapper;
using Employees.Api.Models.Positions;
using Employees.Domain.Aggregates.Positions;
using Employees.Domain.Models;

namespace Employees.Api.AutoMapper.Profiles
{
    public class PositionProfile : Profile
    {
        public PositionProfile()
        {
            CreateMap<Position, PositionModel>();

            CreateMap<GetPositionsModel, PositionFilter>()
                .ForPath(item => item.Pagination.Skip, item => item.MapFrom(m => m.Skip))
                .ForPath(item => item.Pagination.Limit, item => item.MapFrom(m => m.Limit));
        }
    }
}
