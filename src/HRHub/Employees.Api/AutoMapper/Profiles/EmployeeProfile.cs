﻿using AutoMapper;
using Employees.Api.Models.Employees;
using Employees.Domain.Aggregates.Employees;
using Employees.Domain.Models;

namespace Employees.Api.AutoMapper.Profiles
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<Employee, EmployeeInfoModel>();
            CreateMap<EmployeeInfo, EmployeeInfoModel>();

            CreateMap<GetEmployeesModel, EmployeeFilter>()
                .ForPath(item => item.Pagination.Skip, item => item.MapFrom(m => m.Skip))
                .ForPath(item => item.Pagination.Limit, item => item.MapFrom(m => m.Limit));
        }
    }
}
