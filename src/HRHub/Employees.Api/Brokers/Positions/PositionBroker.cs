﻿using AutoMapper;
using Employees.Api.Models.Positions;
using Employees.Api.Services.KeyService;
using Employees.Api.Services.Locker;
using Employees.Domain.Aggregates.Positions;
using Employees.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace Employees.Api.Brokers.Positions
{
    public class PositionBroker : IPositionBroker
    {
        private readonly IPositionRepository positionRepository;
        private readonly IMapper mapper;
        private readonly ISynchronizator synchronizator;
        private readonly IKeyService keyService;

        public PositionBroker(IPositionRepository positionRepository, IMapper mapper, ISynchronizator synchronizator, IKeyService keyService)
        {
            this.positionRepository = positionRepository;
            this.mapper = mapper;
            this.synchronizator = synchronizator;
            this.keyService = keyService;
        }

        public async Task<ActionResult<PositionModel>> CreateAsync(PutPositionModel putModel)
        {
            var position = await positionRepository.InsertAsync(putModel.Grade, putModel.Name);

            var model = mapper.Map<PositionModel>(position);

            return new OkObjectResult(model);
        }

        public async Task<ActionResult<PositionModel[]>> GetAsync(GetPositionsModel getModel)
        {
            var filter = mapper.Map<PositionFilter>(getModel);

            var positions = await positionRepository.GetAsync(filter);

            var models = positions.Select(mapper.Map<PositionModel>).ToArray();

            return new OkObjectResult(models);
        }

        public async Task<ActionResult<PositionModel>> GetAsync(Guid id)
        {
            var position = await positionRepository.GetAsync(id);

            return position == null ? new NotFoundResult() : new OkObjectResult(mapper.Map<PositionModel>(position));
        }

        public async Task<ActionResult<PositionModel>> UpdateAsync(Guid id, PutPositionModel putModel)
        {
            var position = await positionRepository.UpdateAsync(new UpdatePositionAggregate(id, putModel.Grade, putModel.Name));

            return position == null ? new NotFoundResult() : new OkObjectResult(mapper.Map<PositionModel>(position));
        }

        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            await using var _ = await synchronizator.LockAsync(keyService.GetPositionIdKey(id));

            var inUse = await positionRepository.InUseAsync(id);
            if (inUse)
                return new ConflictResult();

            var deleted = await positionRepository.DeleteAsync(id);

            return deleted ? new NoContentResult() : new NotFoundResult();
        }
    }
}
