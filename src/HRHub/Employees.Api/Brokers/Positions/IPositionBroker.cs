﻿using Employees.Api.Models.Positions;
using Microsoft.AspNetCore.Mvc;

namespace Employees.Api.Brokers.Positions
{
    public interface IPositionBroker
    {
        Task<ActionResult<PositionModel>> CreateAsync(PutPositionModel putModel);
        Task<ActionResult> DeleteAsync(Guid id);
        Task<ActionResult<PositionModel[]>> GetAsync(GetPositionsModel getModel);
        Task<ActionResult<PositionModel>> GetAsync(Guid id);
        Task<ActionResult<PositionModel>> UpdateAsync(Guid id, PutPositionModel putModel);
    }
}