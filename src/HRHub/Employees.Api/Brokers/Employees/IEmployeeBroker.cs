﻿using Employees.Api.Models.Employees;
using Microsoft.AspNetCore.Mvc;

namespace Employees.Api.Brokers.Employees
{
    public interface IEmployeeBroker
    {
        Task<ActionResult<EmployeeInfoModel>> CreateAsync(PutEmployeeModel putModel);
        Task<ActionResult> DeleteAsync(Guid id);
        Task<ActionResult<EmployeeInfoModel>> GetAsync(Guid id);
        Task<ActionResult<EmployeeInfoModel[]>> GetAsync(GetEmployeesModel getModel);
        Task<ActionResult<EmployeeInfoModel>> UpdateAsync(Guid id, PutEmployeeModel putModel);
    }
}