﻿using AutoMapper;
using Employees.Api.Models.Employees;
using Employees.Api.Models.Positions;
using Employees.Api.Services.KeyService;
using Employees.Api.Services.Locker;
using Employees.Domain.Aggregates.Employees;
using Employees.Domain.Repositories;
using Employees.Domain.ValueObjects;
using Microsoft.AspNetCore.Mvc;

namespace Employees.Api.Brokers.Employees
{
    public class EmployeeBroker : IEmployeeBroker
    {
        private readonly ISynchronizator synchronizator;
        private readonly IEmployeeRepository employeeRepository;
        private readonly IPositionRepository positionRepository;
        private readonly IMapper mapper;
        private readonly IKeyService keyService;

        public EmployeeBroker(IEmployeeRepository employeeRepository, IMapper mapper, IPositionRepository positionRepository, ISynchronizator synchronizator, IKeyService keyService)
        {
            this.employeeRepository = employeeRepository;
            this.mapper = mapper;
            this.positionRepository = positionRepository;
            this.synchronizator = synchronizator;
            this.keyService = keyService;
        }

        public async Task<ActionResult<EmployeeInfoModel>> CreateAsync(PutEmployeeModel putModel)
        {
            var aggreate = new CreateEmployeeAggregate(putModel.FirstName, putModel.LastName, putModel.Surname, new PossitionIdCollection(putModel.PositionIds));

            var positionIdKeys = keyService.GetPositionIdKeys(putModel.PositionIds).ToArray();

            await using var positionsLocker = await synchronizator.LockAsync(positionIdKeys);

            var employee = await employeeRepository.InsertAsync(aggreate);
            var positions = (await positionRepository.GetAsync(putModel.PositionIds)).ToArray();

            var employeeModel = mapper.Map<EmployeeInfoModel>(employee);
            var positionModels = positions.Select(mapper.Map<PositionModel>).ToArray();
            employeeModel.Positions = positionModels;

            return new CreatedResult($"{employeeModel.Id}", employeeModel);
        }

        public async Task<ActionResult<EmployeeInfoModel>> UpdateAsync(Guid id, PutEmployeeModel putModel)
        {
            var aggregate = new UpdateEmployeeAggregate(
                id,
                putModel.FirstName,
                putModel.LastName,
                putModel.Surname,
                new PossitionIdCollection(putModel.PositionIds));

            await using var employeeLocker = await synchronizator.LockAsync(keyService.GetEmployeeLockKey(id));

            var positionIdKeys = keyService.GetPositionIdKeys(putModel.PositionIds).ToArray();

            await using var positionsLocker = await synchronizator.LockAsync(positionIdKeys);

            var emplopyee = await employeeRepository.UpdateAsync(aggregate);
            if (emplopyee == null)
                return new NotFoundResult();

            var positions = await positionRepository.GetAsync(putModel.PositionIds);

            var employeeModel = mapper.Map<EmployeeInfoModel>(emplopyee);
            employeeModel.Positions = positions.Select(mapper.Map<PositionModel>).ToArray();

            return new OkObjectResult(employeeModel);
        }

        public async Task<ActionResult<EmployeeInfoModel>> GetAsync(Guid id)
        {
            var employee = await employeeRepository.GetAsync(id, true);

            return employee == null ? new NotFoundResult() : new OkObjectResult(mapper.Map<EmployeeInfoModel>(employee));
        }

        public async Task<ActionResult<EmployeeInfoModel[]>> GetAsync(GetEmployeesModel getModel)
        {
            var filter = mapper.Map<EmployeeFilter>(getModel);

            var employees = await employeeRepository.GetAsync(filter, true);

            return employees.Select(mapper.Map<EmployeeInfoModel>).ToArray();
        }

        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            await using var _ = await synchronizator.LockAsync(keyService.GetEmployeeLockKey(id));

            var deleted = await employeeRepository.DeleteAsync(id);

            return deleted ? new NoContentResult() : new NotFoundResult();
        }
    }
}
