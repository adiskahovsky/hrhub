﻿using Employees.Api.Brokers.Positions;
using Employees.Api.Models.Positions;
using Microsoft.AspNetCore.Mvc;

namespace Employees.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionsController : ControllerBase
    {
        private readonly IPositionBroker positionBroker;

        public PositionsController(IPositionBroker positionBroker)
        {
            this.positionBroker = positionBroker;
        }

        [HttpGet]
        public Task<ActionResult<PositionModel[]>> Get([FromQuery] GetPositionsModel getModel)
        {
            return positionBroker.GetAsync(getModel);
        }

        [HttpGet("{id}")]
        public Task<ActionResult<PositionModel>> Get(Guid id)
        {
            return positionBroker.GetAsync(id);
        }

        [HttpPut("{id}")]
        public Task<ActionResult<PositionModel>> Put([FromRoute] Guid id, [FromBody] PutPositionModel putModel)
        {
            return positionBroker.UpdateAsync(id, putModel);
        }

        [HttpPut]
        public async Task<ActionResult<PositionModel>> Put([FromBody] PutPositionModel putModel)
        {
            return await positionBroker.CreateAsync(putModel);
        }

        [HttpDelete("{id}")]
        public Task<ActionResult> Delete(Guid id)
        {
            return positionBroker.DeleteAsync(id);
        }
    }
}
