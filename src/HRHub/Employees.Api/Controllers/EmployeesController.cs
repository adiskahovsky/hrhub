﻿using Employees.Api.Brokers.Employees;
using Employees.Api.Models.Employees;
using Microsoft.AspNetCore.Mvc;

namespace Employees.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeBroker employeeBroker;

        public EmployeesController(IEmployeeBroker employeeBroker)
        {
            this.employeeBroker = employeeBroker;
        }

        [HttpGet("{id}")]
        public Task<ActionResult<EmployeeInfoModel>> Get([FromRoute] Guid id)
        {
            return employeeBroker.GetAsync(id);
        }

        [HttpGet]
        public Task<ActionResult<EmployeeInfoModel[]>> Get([FromQuery] GetEmployeesModel getModel)
        {
            return employeeBroker.GetAsync(getModel);
        }

        [HttpPut("{id}")]
        public Task<ActionResult<EmployeeInfoModel>> Put([FromRoute] Guid id, [FromBody] PutEmployeeModel putModel)
        {
            return employeeBroker.UpdateAsync(id, putModel);
        }

        [HttpPut]
        public Task<ActionResult<EmployeeInfoModel>> Put([FromBody] PutEmployeeModel putModel)
        {
            return employeeBroker.CreateAsync(putModel);
        }

        [HttpDelete("{id}")]
        public Task<ActionResult> Delete(Guid id)
        {
            return employeeBroker.DeleteAsync(id);
        }
    }
}
