using Employees.Api.Brokers.Employees;
using Employees.Api.Brokers.Positions;
using Employees.Api.Services.KeyService;
using Employees.Api.Services.Locker;
using Employees.Infrastructure.Setup;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.Configure<DatabaseOptions>(builder.Configuration.GetRequiredSection(nameof(DatabaseOptions)).Bind);

builder.Services
    .AddInfrastructure()
    .AddScoped<ISynchronizator, SemaphoreSynchronizator>()
    .AddScoped<IEmployeeBroker, EmployeeBroker>()
    .AddScoped<IPositionBroker, PositionBroker>()
    .AddScoped<ISynchronizator, SemaphoreSynchronizator>()
    .AddSingleton<IKeyService, KeyService>()
    .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies(), ServiceLifetime.Scoped)
    .AddLogging();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
