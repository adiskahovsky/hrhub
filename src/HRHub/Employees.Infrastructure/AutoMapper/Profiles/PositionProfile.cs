﻿using AutoMapper;
using Employees.Domain.Models;
using Employees.Infrastructure.Entities;
using System.Collections.Generic;

namespace Employees.Infrastructure.AutoMapper.Profiles
{
    internal class PositionProfile : Profile
    {
        public PositionProfile()
        {
            CreateMap<PositionEntity, Position>();
        }
    }
}
