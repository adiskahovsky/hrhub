﻿using AutoMapper;
using Employees.Domain.Aggregates.Employees;
using Employees.Domain.Models;
using Employees.Infrastructure.Entities;

namespace Employees.Infrastructure.AutoMapper.Profiles
{
    internal class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeEntity, Employee>();
            CreateMap<EmployeeEntity, EmployeeInfo>();
        }
    }
}
