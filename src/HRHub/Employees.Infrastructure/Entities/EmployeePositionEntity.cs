﻿namespace Employees.Infrastructure.Entities
{
    internal class EmployeePositionEntity
    {
        public long Id { get; init; }
        public required Guid EmployeeId { get; init; }
        public required Guid PositionId { get; init; }

        public EmployeeEntity Employee { get; init; }
        public PositionEntity Position { get; init; }
    }
}
