﻿namespace Employees.Infrastructure.Entities
{
    internal class EmployeeEntity
    {
        public required Guid Id { get; init; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Surname { get; set; }

        public ICollection<PositionEntity> Positions { get; init; }
    }
}
