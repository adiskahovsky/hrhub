﻿using Employees.Domain.ValueObjects;

namespace Employees.Infrastructure.Entities
{
    internal class PositionEntity
    {
        public required Guid Id { get; init; }
        public required Grade Grade { get; set; }
        public required string Name { get; set; }
        public ICollection<EmployeeEntity> Employees { get; init; }
    }
}
