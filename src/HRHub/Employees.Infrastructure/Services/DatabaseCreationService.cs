﻿using Microsoft.Extensions.Hosting;

namespace Employees.Infrastructure.Services
{
    internal class DatabaseCreationService : BackgroundService
    {
        private readonly Context context;

        public DatabaseCreationService(Context context)
        {
            this.context = context;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return context.Database.EnsureCreatedAsync(stoppingToken);
        }
    }
}
