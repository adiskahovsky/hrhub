﻿using Employees.Infrastructure.Converters;
using Employees.Infrastructure.Entities;
using Employees.Infrastructure.Setup;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Employees.Infrastructure
{
    internal class Context : DbContext
    {
        private readonly DatabaseOptions options;

        public Context(IOptions<DatabaseOptions> options)
        {
            this.options = options.Value;
        }

        public DbSet<EmployeeEntity> Employees { get; init; }
        public DbSet<PositionEntity> Positions { get; init; }

        public DbSet<EmployeePositionEntity> EmployeePositions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<PositionEntity>()
                .Property(item => item.Grade)
                .HasConversion<GradeConverter>();

            modelBuilder
                .Entity<EmployeeEntity>()
                .HasMany(item => item.Positions)
                .WithMany(item => item.Employees)
                .UsingEntity<EmployeePositionEntity>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(options.ConnectionString);
        }
    }
}
