﻿using Employees.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Employees.Infrastructure.Converters
{
    internal class GradeConverter : ValueConverter<Grade, int>
    {
        public GradeConverter() : base(item => item.Value, item => new Grade(item))
        {
        }
    }
}
