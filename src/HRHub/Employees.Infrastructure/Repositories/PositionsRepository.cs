﻿using AutoMapper;
using Employees.Domain.Aggregates.Positions;
using Employees.Domain.Models;
using Employees.Domain.Repositories;
using Employees.Domain.ValueObjects;
using Employees.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace Employees.Infrastructure.Repositories
{
    internal class PositionsRepository : IPositionRepository
    {
        private readonly Context context;
        private readonly IMapper mapper;

        public PositionsRepository(Context context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<Position> InsertAsync(Grade grade, string name)
        {
            var entity = new PositionEntity()
            {
                Id = Guid.NewGuid(),
                Grade = grade,
                Name = name
            };

            await context.Positions.AddAsync(entity).ConfigureAwait(false);
            await context.SaveChangesAsync().ConfigureAwait(false);

            var position = mapper.Map<Position>(entity);

            return position;
        }

        public async Task<Position?> GetAsync(Guid id)
        {
            var entity = await context.Positions.FirstOrDefaultAsync(item => item.Id == id).ConfigureAwait(false);
            if (entity == null)
                return null;

            var position = mapper.Map<Position>(entity);

            return position;
        }

        public async Task<IEnumerable<Position>> GetAsync(PositionFilter filter)
        {
            var query = context.Positions.AsNoTracking();
            if (filter.Pagination.Skip != null)
                query = query.Skip(filter.Pagination.Skip.Value);

            var entities = await query.Take(filter.Pagination.Limit).ToArrayAsync().ConfigureAwait(false);

            return entities.Select(mapper.Map<Position>);
        }

        public async Task<IEnumerable<Position>> GetAsync(Guid[] ids)
        {
            var entities = await context.Positions.Where(item => ids.Contains(item.Id)).ToArrayAsync().ConfigureAwait(false);

            var positions = entities.Select(mapper.Map<Position>).ToArray();

            return positions;
        }

        public async Task<Position> UpdateAsync(UpdatePositionAggregate aggregate)
        {
            var entity = await context.Positions.FirstOrDefaultAsync(item => item.Id == aggregate.Id).ConfigureAwait(false);
            if (entity == null)
                return null;

            if (aggregate.Name != null)
                entity.Name = aggregate.Name;

            if (aggregate.Grade != null)
                entity.Grade = aggregate.Grade.Value;

            context.Positions.Update(entity);
            await context.SaveChangesAsync().ConfigureAwait(false);

            var position = mapper.Map<Position>(entity);
            return position;
        }

        public Task<bool> InUseAsync(Guid id)
        {
            return context.EmployeePositions.AsNoTracking().AnyAsync(item => item.PositionId == id);
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var deleted = await context.Positions.Where(item => item.Id == id).ExecuteDeleteAsync().ConfigureAwait(false) == 1;
            if (!deleted)
                return false;

            return deleted;
        }
    }
}
