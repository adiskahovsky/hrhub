﻿using AutoMapper;
using Employees.Domain.Aggregates.Employees;
using Employees.Domain.Models;
using Employees.Domain.Repositories;
using Employees.Domain.ValueObjects;
using Employees.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Employees.Infrastructure.Repositories
{
    internal class EmployeeRepository : IEmployeeRepository
    {
        private readonly Context context;
        private readonly IMapper mapper;

        public EmployeeRepository(Context context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<EmployeeInfo> GetAsync(Guid Id, bool withPositions)
        {
            var query = context.Employees.AsNoTracking();

            if (withPositions)
                query = query.Include(item => item.Positions);

            var entity = await query.FirstOrDefaultAsync(item => item.Id == Id).ConfigureAwait(false);

            var employee = mapper.Map<EmployeeInfo>(entity);

            return employee;
        }

        public async Task<IEnumerable<EmployeeInfo>> GetAsync(EmployeeFilter filter, bool withPositions)
        {
            var query = context.Employees.AsNoTracking();

            if (filter.Pagination.Skip != null)
                query = query.Skip(filter.Pagination.Skip.Value);

            if (withPositions)
                query = query.Include(item => item.Positions);

            query = query.Take(filter.Pagination.Limit);

            var entities = await query.ToArrayAsync().ConfigureAwait(false);

            return entities.Select(mapper.Map<EmployeeInfo>);

        }

        public async Task<Employee> InsertAsync(CreateEmployeeAggregate aggregate)
        {
            var entity = new EmployeeEntity()
            {
                Id = Guid.NewGuid(),
                FirstName = aggregate.FirstName,
                LastName = aggregate.LastName,
                Surname = aggregate.Surname,
                Positions = new List<PositionEntity>(aggregate.PositionIds.Count())
            };
            await context.Employees.AddAsync(entity).ConfigureAwait(false);

            var connections = aggregate.PositionIds.Select(item => new EmployeePositionEntity() { EmployeeId = entity.Id, PositionId = item }).ToArray();
            await context.EmployeePositions.AddRangeAsync(connections).ConfigureAwait(false);

            await context.SaveChangesAsync().ConfigureAwait(false);

            var employee = mapper.Map<Employee>(entity);

            return employee;
        }

        public async Task<Employee> UpdateAsync(UpdateEmployeeAggregate aggregate)
        {
            var entity = await context.Employees.FirstOrDefaultAsync(item => item.Id == aggregate.Id).ConfigureAwait(false);
            if (entity == null)
                return null;

            if (aggregate.FirstName != null)
                entity.FirstName = aggregate.FirstName;

            if (aggregate.LastName != null)
                entity.LastName = aggregate.LastName;

            if (aggregate.Surname != null)
                entity.Surname = aggregate.Surname;

            context.Employees.Update(entity);

            if (aggregate.PositionIds != null)
                await PutPositionsAsync(entity.Id, aggregate.PositionIds).ConfigureAwait(false);

            await context.SaveChangesAsync().ConfigureAwait(false);

            var employee = mapper.Map<Employee>(entity);

            return employee;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            await using var transaction = await context.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted).ConfigureAwait(false);

            var deleted = await context.Employees.Where(item => item.Id == id).ExecuteDeleteAsync().ConfigureAwait(false) == 1;
            if (!deleted)
                return false;

            await context.EmployeePositions.Where(item => item.EmployeeId == id).ExecuteDeleteAsync().ConfigureAwait(false);

            await transaction.CommitAsync().ConfigureAwait(false);

            return true;
        }

        private async Task PutPositionsAsync(Guid employeeId, PossitionIdCollection positionIds)
        {
            await context.EmployeePositions
                    .Where(item => item.EmployeeId == employeeId)
                    .Where(item => !positionIds.Contains(item.PositionId))
                    .ExecuteDeleteAsync()
                    .ConfigureAwait(false);

            var employeePositionIds = await context.EmployeePositions
                .Where(item => item.EmployeeId == employeeId)
                .Select(item => item.PositionId)
                .ToArrayAsync()
                .ConfigureAwait(false);

            foreach (var positionId in positionIds)
            {
                if (employeePositionIds.Contains(positionId))
                    continue;

                await context.EmployeePositions.AddAsync(new EmployeePositionEntity()
                {
                    EmployeeId = employeeId,
                    PositionId = positionId
                }).ConfigureAwait(false);
            }
        }
    }
}
