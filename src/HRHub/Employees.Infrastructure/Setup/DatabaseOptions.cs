﻿namespace Employees.Infrastructure.Setup
{
    public class DatabaseOptions
    {
        public required string ConnectionString { get; init; }
    }
}
