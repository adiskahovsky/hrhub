﻿using Employees.Domain.Repositories;
using Employees.Infrastructure.Repositories;
using Employees.Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Employees.Infrastructure.Setup
{
    public static class DIExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            return services
                .AddHostedService<DatabaseCreationService>()
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies(), ServiceLifetime.Scoped)
                .AddDbContext<Context>(ServiceLifetime.Transient)
                .AddScoped<IEmployeeRepository, EmployeeRepository>()
                .AddScoped<IPositionRepository, PositionsRepository>();
        }
    }
}
