﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Employees
{
    public class EmployeeModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string? Surname { get; set; }
    }
}
