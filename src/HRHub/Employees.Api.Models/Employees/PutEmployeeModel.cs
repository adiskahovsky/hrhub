﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Employees
{
    public class PutEmployeeModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string? Surname { get; set; }

        [Required]
        [MinLength(1)]
        public Guid[] PositionIds { get; set; }
    }
}
