﻿using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Employees
{
    public class GetEmployeesModel
    {
        public int? Skip { get; set; }

        [Required]
        [Range(1, 20)]
        public int Limit { get; set; }
    }
}
