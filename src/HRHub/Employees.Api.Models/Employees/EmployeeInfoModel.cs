﻿using Employees.Api.Models.Positions;
using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Employees
{
    public class EmployeeInfoModel : EmployeeModel
    {
        [Required]
        [MinLength(1)]
        public PositionModel[] Positions { get; set; }
    }
}
