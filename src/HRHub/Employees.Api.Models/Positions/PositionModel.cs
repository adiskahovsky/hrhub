﻿using Employees.Domain.Constants;
using System;
using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Positions
{
    public class PositionModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(GradeConstants.MinGrade, GradeConstants.MaxGrade)]
        public int Grade { get; set; }
    }
}
