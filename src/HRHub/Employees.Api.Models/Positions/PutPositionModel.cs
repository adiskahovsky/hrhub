﻿using Employees.Domain.Constants;
using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Positions
{
    public class PutPositionModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(GradeConstants.MinGrade, GradeConstants.MaxGrade)]
        public int Grade { get; set; }
    }
}
