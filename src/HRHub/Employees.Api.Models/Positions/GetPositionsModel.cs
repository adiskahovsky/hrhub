﻿using System.ComponentModel.DataAnnotations;

namespace Employees.Api.Models.Positions
{
    public class GetPositionsModel
    {
        public int? Skip { get; set; }

        [Required]
        [Range(1, 50)]
        public int Limit { get; set; }
    }
}
