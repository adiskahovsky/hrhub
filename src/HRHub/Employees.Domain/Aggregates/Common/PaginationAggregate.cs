﻿namespace Employees.Domain.Aggregates.Common
{
    public record PaginationAggregate
    {
        public int? Skip { get; init; }
        public int Limit { get; init; }
    }
}
