﻿using Employees.Domain.Aggregates.Common;

namespace Employees.Domain.Aggregates.Positions
{
    public record PositionFilter
    {
        public required PaginationAggregate Pagination { get; init; }
    }
}
