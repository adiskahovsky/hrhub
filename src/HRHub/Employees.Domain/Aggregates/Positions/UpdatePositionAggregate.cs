﻿using Employees.Domain.ValueObjects;

namespace Employees.Domain.Aggregates.Positions
{
    public record UpdatePositionAggregate(Guid Id, Grade? Grade, string? Name);
}
