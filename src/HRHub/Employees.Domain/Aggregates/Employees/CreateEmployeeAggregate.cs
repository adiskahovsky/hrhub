﻿using Employees.Domain.ValueObjects;

namespace Employees.Domain.Aggregates.Employees
{
    public record CreateEmployeeAggregate(string FirstName, string LastName, string? Surname, PossitionIdCollection PositionIds);
}
