﻿using Employees.Domain.Models;

namespace Employees.Domain.Aggregates.Employees
{
    public record EmployeeInfo
    {
        public Guid Id { get; init; }
        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string? Surname { get; init; }
        public Position[]? Positions { get; init; }
    }
}
