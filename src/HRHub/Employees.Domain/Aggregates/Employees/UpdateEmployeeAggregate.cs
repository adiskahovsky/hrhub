﻿using Employees.Domain.ValueObjects;

namespace Employees.Domain.Aggregates.Employees
{
    public record UpdateEmployeeAggregate(Guid Id, string? FirstName, string? LastName, string? Surname, PossitionIdCollection? PositionIds);
}
