﻿using Employees.Domain.Aggregates.Common;

namespace Employees.Domain.Aggregates.Employees
{
    public class EmployeeFilter
    {
        public required PaginationAggregate Pagination { get; init; }
    }
}
