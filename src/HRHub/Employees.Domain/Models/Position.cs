﻿using Employees.Domain.ValueObjects;

namespace Employees.Domain.Models
{
    public record Position(Guid Id, string Name, Grade Grade);
}
