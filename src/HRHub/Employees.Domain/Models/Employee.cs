﻿namespace Employees.Domain.Models
{
    public record Employee(Guid Id, string FirstName, string LastName, string? Surname);
}
