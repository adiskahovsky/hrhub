﻿namespace Employees.Domain.Exceptions
{
    public class DomainLogicException : Exception
    {
        public DomainLogicException(string message) : base(message)
        {
        }
    }
}
