﻿using Employees.Domain.Aggregates.Positions;
using Employees.Domain.Models;
using Employees.Domain.ValueObjects;

namespace Employees.Domain.Repositories
{
    public interface IPositionRepository
    {
        Task<Position> InsertAsync(Grade grade, string name);
        Task<Position> GetAsync(Guid Id);
        Task<IEnumerable<Position>> GetAsync(Guid[] Ids);
        Task<IEnumerable<Position>> GetAsync(PositionFilter filter);
        Task<bool> InUseAsync(Guid id);
        Task<Position> UpdateAsync(UpdatePositionAggregate aggregate);
        Task<bool> DeleteAsync(Guid id);
    }
}
