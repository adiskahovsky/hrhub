﻿using Employees.Domain.Aggregates.Employees;
using Employees.Domain.Models;

namespace Employees.Domain.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee> InsertAsync(CreateEmployeeAggregate model);
        Task<IEnumerable<EmployeeInfo>> GetAsync(EmployeeFilter filter, bool withPositions);
        Task<EmployeeInfo> GetAsync(Guid Id, bool withPositions);
        Task<Employee> UpdateAsync(UpdateEmployeeAggregate aggregate);
        Task<bool> DeleteAsync(Guid id);
    }
}
