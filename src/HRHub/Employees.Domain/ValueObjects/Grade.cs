﻿using Employees.Domain.Constants;
using Employees.Domain.Exceptions;

namespace Employees.Domain.ValueObjects
{
    public struct Grade
    {
        public static implicit operator int(Grade grade) => grade.Value;
        public static implicit operator Grade(int gradeValue) => new Grade(gradeValue);

        public static void Validate(int value)
        {
            if (value < GradeConstants.MinGrade)
                throw new DomainLogicException($"{nameof(value)} cannot be less then {GradeConstants.MinGrade}");

            if (value > GradeConstants.MaxGrade)
                throw new DomainLogicException($"{nameof(value)} cannot be more then {GradeConstants.MaxGrade}");
        }

        public Grade(int value)
        {
            Validate(value);

            Value = value;
        }

        public int Value { get; }
    }
}
