﻿using Employees.Domain.Exceptions;
using System.Collections;

namespace Employees.Domain.ValueObjects
{
    public class PossitionIdCollection : IEnumerable<Guid>
    {
        public static void Validate(IEnumerable<Guid> positions)
        {
            if (positions.Any() == false)
                throw new DomainLogicException("Employee's positions cannot be empty.");
        }

        private readonly HashSet<Guid> positions;

        public PossitionIdCollection(IEnumerable<Guid> source)
        {
            Validate(source);

            positions = source.ToHashSet();
        }

        public IEnumerator<Guid> GetEnumerator() => positions.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => positions.GetEnumerator();
    }
}
