﻿namespace Employees.Domain.Constants
{
    public class GradeConstants
    {
        public const int MinGrade = 1;
        public const int MaxGrade = 15;
    }
}
